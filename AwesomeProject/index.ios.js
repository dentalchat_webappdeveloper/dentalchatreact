"use strict";

import {AppRegistry} from 'react-native';
import React from 'react'
import App from './src/App'
import PropTypes from 'prop-types';

AppRegistry.registerComponent('AwesomeProject', () => App);
