/* @flow */

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Alert,
  Text,
  Button
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import PatientMessageItem from '../components/PatientMessageItem';
import Spinner from 'react-native-loading-spinner-overlay';
import MessagesIconWithBadge from '../components/MessagesIconWithBadge';
import SInfo from 'react-native-sensitive-info';
import Patient from './PatientMessages';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import PropTypes from 'prop-types';
export default class PatientMessages extends Component {

  
  static navigationOptions = {
    title: 'Message',
  };

 

  constructor(props) {
    super(props);
    global.MyVar = '0';

    this.state = {
      data:[],
      visible: true,
      rady: false,
      dataArray: [],
      dataArrayAll: [],
      patientId: '',
      post_id: '',
      isRefreshing: false,
      isSearching: false,
      alertMsg: '',
      unCounts: '0',
      more: '0',
      page: 0,
      myver:'11'
    };
  }

 



  componentWillMount() {
    this.setState({
      page: 0,
      dataArray: [],
      dataArrayAll: [],
    })
    SInfo.getItem('patient_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      this.setState({ patientId: value, })
    });
    SInfo.getItem('post_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      console.log('WWWWWW= ' + value);
      if (value == '' || value == undefined) {
        value = "";
      }
      this.setState({ post_id: value, })
      this.mGetRecentPost();

    });
  }

  mLoaderShowHide() {
    this.setState({
      visible: !this.state.visible,
      rady: !this.state.rady
    });
  };

  mFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'Something went wrong, Please try again');
      }, 200);
    });
  }

  mGetRecentPost() {
    const { navigation } = this.props;
    var mThis = this;
    var rawData = [];
    var mUnreadCount = 0;

    fetch('https://blog.dentalchat.com/server/get-recent-post', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        auth_token:"",
        patient_id:this.state.patientId,
        post_id:this.state.post_id,
        is_read:'1',
        page:this.state.page,
        perpage:'13',
      }),
    }).then((response) => response.json())
        .then((responseJson) => {
          //var users = responseJson;
         
          for (i in responseJson.patient_post_arr) {
            if (responseJson.patient_post_arr[i].all_unread_chat_count_for_this_post > 0) {
              if (responseJson.patient_post_arr[i].chat_history_arr.doctor_content.length > 0) {
                mUnreadCount = mUnreadCount + responseJson.patient_post_arr[i].all_unread_chat_count_for_this_post;
                console.log('<><><>###call ' + mUnreadCount);
                mThis.setState({
                  unCounts: mUnreadCount
                });
  
              }
            }
            if (responseJson.patient_post_arr[i].chat_history_arr.get_doctor == null) {
            } else {
              rawData.push(responseJson.patient_post_arr[i])
            }
          }
          mThis.setState(state => ({
            dataArray: [...state.dataArray,...rawData],
            dataArrayAll: [...state.dataArrayAll,...rawData],
            post_id: '',
            alertMsg: 'Sorry! No messages available',
            more: responseJson.more,
          }));

           mThis.mLoaderShowHide();
           console.log("<><><> "+mThis.state.unCounts)
           global.MyVar = mThis.state.unCounts;
           mThis.props.navigation.setParams({ handleSave:mThis.state.unCounts});
           SInfo.setItem('post_id', '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });


          //console.log("<><><>akki "+responseJson);
        })
        .catch((error) => {
          console.error(error);
          mThis.setState({
            rady: true,
            post_id: '',
            alertMsg: 'Something went wrong, Please try again'
          });
          SInfo.setItem('post_id', '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          mThis.mFailed();
        });

  }





  mSearchFilterFunction(text) {
    const newData = this.state.dataArray.filter(function (item) {
      const itemData = item.post_title.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1
    })
    if (!text || text === '') {
      console.log('<><><><><>change state');
      this.setState({
        dataArray: this.state.dataArrayAll,
        text: text,
        isSearching: false,
      });
    }
    else if (!Array.isArray(newData) && !newData.length) {
      this.setState({
        data: [],
        isSearching: true,
      });
    } else {
      this.setState({
        dataArray: newData,
        text: text,
        isSearching: true,
      })
    }
  }



  onScrollHandler = () => {
    if (this.state.more == 1 && this.state.isSearching == false) {

      this.setState({
        page: this.state.page + 10,
        rady: false,
        visible: true,
     }, () => {

      SInfo.getItem('patient_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
        this.setState({ patientId: value, })
      });
      SInfo.getItem('post_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
        console.log('WWWWWW= ' + value);
        if (value == '' || value == undefined) {
          value = "";
        }
        this.setState({ post_id: value, })
        this.mGetRecentPost();
      });
     });

    }
   
}





  mRefresh = async () => {
    this.setState({
      page: 0,
      rady: false,
      visible: true,
      dataArray: [],
      dataArrayAll: [],
    });
    SInfo.getItem('patient_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      this.setState({ patientId: value, })
      this.mGetRecentPost();

    });

  };



  renderHeader = () => {
    return (




      <SearchBar
        placeholder="Search here ..."
        containerStyle={{ backgroundColor: 'white' }}
        inputStyle={{ color: '#000' }}
        lightTheme
        onChangeText={(text) => this.mSearchFilterFunction(text)}
        value={this.state.text}
      />
    );
  };
  



  render() {
    const { navigate } = this.props.navigation;

       return (
        <View style={styles.container}>
        {this.state.rady==false ? <Spinner overlayColor={'rgba(0, 0, 0, 0.75)'} color={'#08a1d9'} textContent={"Updating"} visible={this.state.visible} textStyle={{ color: '#fff', fontSize: 15, marginTop: -70 }} /> : <View>
            </View> } 
              <FlatList   
            data={this.state.dataArray}
            keyExtractor={item => item.chat_history_arr.id}
            ListHeaderComponent={this.renderHeader}
            renderItem={({ item }) => <PatientMessageItem item={item} onPress={() => navigate('PatientChatWindow', item)} />}
            onRefresh={this.mRefresh}
            refreshing={this.state.isRefreshing}
            onEndReached={this.onScrollHandler}
            onEndReachedThreshold={0}
          />

        </View>
      );
   

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
