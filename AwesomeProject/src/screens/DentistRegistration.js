/* @flow */

import React, { Component } from 'react';
import { ActivityIndicator, Alert, TextInput, Text, Picker, ScrollView, Linking, TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';
import Iconss from 'react-native-vector-icons/dist/FontAwesome';
import CheckBox from 'react-native-checkbox';
import ModalDropdown from 'react-native-modal-dropdown';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import PropTypes from 'prop-types';


import {
  View,
  StyleSheet
} from 'react-native';
import TextInputWithIcon from '../components/TextInputWithIcon';
import Button from '../components/Button';
import SInfo from 'react-native-sensitive-info'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'


const DEMO_OPTIONS_1 = ['Male', 'Female', 'Rather not say'];

export default class DentistRegistration extends Component {
  static navigationOptions = {
    header: null
    };


  mValidateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      firstname: '',
      lastname: '',
      businessname: '',
      countrycode: '',
      phone: '',
      password: '',
      zipcode: '',
      email: '',
      checkBox1: false,
      checkBox2: false,
    };
  }




  mLoaderShowHide() {
    this.setState({
      visible: !this.state.visible
    });
  };

  mValidation() {
    if (this.state.firstname.length <= 0) {
      Alert.alert('First name field is required..')
      return false;
    } else if (this.state.lastname.length <= 0) {
      Alert.alert('Last name field is required..')
      return false;
    } else if (this.state.businessname.length <= 0) {
      Alert.alert('Business name field is required..')
      return false;
    } else if (this.state.countrycode.length <= 0) {
      Alert.alert('Country code field is required..')
      return false;
    } else if (this.state.phone.length <= 0) {
      Alert.alert('Phone Number field is required..')
      return false;
    } else if (this.state.email.length <= 0) {
      Alert.alert('Email address field is required..')
      return false;
    } else if (!this.mValidateEmail(this.state.email)) {
      Alert.alert('Please enter a valid email address.')
      return false;
    } else if (this.state.password.length <= 0) {
      Alert.alert('Password field is required..')
      return false;
    } else if (this.state.zipcode.length <= 0) {
      Alert.alert('Zipcode field is required..')
      return false;
    } else if (this.state.checkBox1 == false) {
      Alert.alert('Please select privacy policy')
      return false;
    } else if (this.state.checkBox2 == false) {
      Alert.alert('Please select HIPAA Authorization')
      return false;
    }
    this.mLoaderShowHide();
    this.mRegistration();
  }

  mFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'Email address already exist');
      }, 200);
    });
  }

  mSuccess() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Success', 'Thank you for registering your Dental Practice with us A verification link has been sent to email id ' + this.state.email);
      }, 200);
    });
  }

  mNetworkFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'The Internet connection appears to be offline, Please try again');
      }, 200);
    });
  }



  mPatientEmailCheck() {
    var mThis = this;
    var data = new FormData();
    data.append("email", this.state.email);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://blog.dentalchat.com/server/service/patient/patient-email-check", true);
    xhr.onload = function () {
      var users = xhr.responseText;
      if (xhr.readyState == 4 && xhr.status == "200") {
        console.log('<><><> ' + users);
        var obj = JSON.parse(users);
        if (obj.status == "1") {

        } else {
          mThis.mFailed();
        }
      } else {
        mThis.mNetworkFailed();
        console.log('<><><> ' + users);
      }
    }
    xhr.send(data);
  }



  mRegistration() {

    var mThis = this;
    var data = new FormData();
    data.append("user[first_name]", this.state.firstname);
    data.append("user[last_name]", this.state.lastname);
    data.append("user[business_name]", this.state.businessname);
    data.append("user[contact_number]", this.state.phone);
    data.append("user[password]", this.state.password);
    data.append("user[country_code]", this.state.countrycode);
    data.append("user[email]", this.state.email);
    data.append("user[zipcode]", this.state.zipcode);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://blog.dentalchat.com/server/service/register", true);
    xhr.onload = function () {
      var users = xhr.responseText;
      if (xhr.readyState == 4 && xhr.status == "200") {
        console.log('<><><> ' + users);
        var obj = JSON.parse(users);
        mThis.mSuccess();
        mThis.props.navigation.navigate('DentistLogin')
      } else {
        mThis.mNetworkFailed();
        console.log('<><><> ' + users);
      }
    }
    xhr.send(data);
  }

  mDemo(value) {
    dismissKeyboard()

  }

  mCheckBox1() {
    this.setState({
      checkBox1: !this.state.checkBox1,
    })
  }

  mCheckBox2() {
    this.setState({
      checkBox2: !this.state.checkBox2,
    })
  }


  render() {
    const { navigate } = this.props.navigation;
    const label = (
      <View style={styles.row}>

        <Text>Expiration date</Text>
        <Text>Expiration </Text>



      </View>
    )

    return (

<View style={{backgroundColor: 'white',flex: 1}}>


<View style={{flexDirection:'row'}}>
<View style={{height:60,width:'60%',marginTop:20,marginLeft:'20%',backgroundColor:'#ffffff',justifyContent: 'center', alignItems: 'center'}}>
<Text style={{ fontSize: 18, fontWeight: 'bold' }}>Dentist Registration</Text>
</View>
<View style={{position: "absolute", top: 35, left: 0,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
<View style={{ flexDirection: 'row' }}>
  <Iconss style={{ marginLeft: 15 }} name='angle-left' size={30} color={'#2b94bf'} />
  <Text style={{ fontSize: 15, marginTop: 7, marginLeft: 5, fontWeight: '500', color: '#2b94bf' }}>Back</Text>
</View>
</TouchableOpacity>
</View>
</View>

<View
style={{
borderBottomColor: 'black',
borderBottomWidth: 1,
}}
/>

<KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
        scrollEnabled={true}
      >
        <View style={styles.container}>
          <Spinner overlayColor={'rgba(0, 0, 0, 0.75)'} color={'#08a1d9'} textContent={"Updating"} visible={this.state.visible} textStyle={{ color: '#fff', fontSize: 15, marginTop: -70 }}>
          </Spinner>
          <View>

            <View style={styles.searchSection}>
              <TextInput
                keyboardType='default'
                placeholder='Dentist First Name'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ firstname: text })} />
            </View>

            <View style={styles.lineStyle}></View>

            <View style={styles.searchSection}>
              <TextInput
                keyboardType='default'
                placeholder='Last Name Initial'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ lastname: text })} />
            </View>

            <View style={styles.lineStyle}></View>

            <View style={styles.searchSection}>
              <TextInput
                keyboardType='default'
                placeholder='Business Name'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ businessname: text })} />
            </View>

            <View style={styles.lineStyle}></View>




            <View style={styles.searchSection}>
              <TextInput
              keyboardType='phone-pad'
              placeholder='+1'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ countrycode: text })} />
            </View>

            <View style={styles.lineStyle}></View>



            <View style={styles.searchSection}>
              <TextInput
              keyboardType='phone-pad'
              placeholder='Business Phone'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ phone: text })} />
            </View>

            <View style={styles.lineStyle}></View>  
       

            <View style={styles.searchSection}>
              <TextInput
                keyboardType='default'
                placeholder='Zip Code'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ zipcode: text })} />
            </View>
            <View style={styles.lineStyle}></View>





            <View style={styles.searchSection}>
              <TextInput
                keyboardType='email-address'
                placeholder='Email address'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ email: text })} />
            </View>

            <View style={styles.lineStyle}></View>

            <View style={styles.searchSection}>
              <TextInput
                secureTextEntry={true}
                keyboardType='default'
                placeholder='Password'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ password: text })} />
            </View>

            <View style={styles.lineStyle}></View>


            <View style={styles.searchSection6}>
              <CheckBox
                label=''
                checkboxStyle={{ height: 15, width: 15 }}
                onChange={() => this.mCheckBox1()}
              />
              <Text style={{ marginLeft: 5, fontSize: 15 }}> I agree</Text>
              <Text style={{ marginLeft: 5, fontSize: 15, color: '#337ab7' }} onPress={() => { Linking.openURL('https://dentalchat.com/terms-and-conditions') }}>terms</Text>
              <Text style={{ marginLeft: 5, fontSize: 15 }}>of use and</Text>
              <Text style={{ marginLeft: 5, fontSize: 15, color: '#337ab7' }} onPress={() => { Linking.openURL('https://dentalchat.com/privacy-policy') }}>privacy policy.</Text>
            </View>



            <View style={styles.searchSection6}>
              <CheckBox
                label=''
                checkboxStyle={{ height: 15, width: 15 }}
                onChange={() => this.mCheckBox2()}
              />

              <Text style={{ marginLeft: 5, fontSize: 15 }}> Read and accept</Text>
              <Text style={{ marginLeft: 5, fontSize: 15, color: '#337ab7' }} onPress={() => { Linking.openURL('https://dentalchat.com/hipaa-authorization') }}>Dentalchat HIPAA Authorization</Text>
            </View>




          </View>
          <View style={styles.searchSection2}>
            <Button
              buttonType="logInButton"
              name='Save & Continue'
              onPress={() => this.mValidation()} />
          </View>
        </View>
      </KeyboardAwareScrollView>

</View>

      



    );
  }
}

const styles = StyleSheet.create({


  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    backgroundColor: 'white'
  },
  chaildView: {
    width: 60,
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: '#000'
  },

  chaildView2: {
    marginLeft: 20,
    width: 300,
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: '#000'
  },
  row: {
    width: 200,
    height: 20,
    flex: 1,
    flexDirection: "row"
  },
  dropdown: {
    flex: 1,
    top: 5,
  },
  lineStyle: {
    
    borderWidth: 0.5,
    borderColor: 'black',
    marginLeft: 20,
    marginRight: 20,
  },
  lineStyle2: {
    width: 60,
    borderWidth: 0.5,
    borderColor: 'black',
  },

  lineStyle3: {
    width: 184,
    borderWidth: 0.5,
    borderColor: 'black',
  },
  dropdown_row: {
    flexDirection: 'row',
  },
  dropdown_cell: {
    shadowOffset: { width: 1, height: 2 },
    shadowColor: '#ccc',
    width: 200,
    height: 50,
    borderColor: '#ccc'
  },
  dropdown_text: {
    marginVertical: 5,
    fontSize: 18,
    color: '#111',
    textAlignVertical: 'center',
  },
  dropdown_pop: {
    width: 200,
    height: 160,
  },
  dropdown_pop_text: {
    marginVertical: 5,
    marginHorizontal: 6,
    fontSize: 18,
    color: '#ccc',
  },

  buttonContainerStyle: {
    marginTop: 30
  },
  textInputStyle: {
    width: "90%",
    height: 50,
    marginLeft: 20,
    marginRight: 20,
    fontSize: 17,
    fontWeight: '500',
    borderColor: '#ccc'
  },
  textInputStyle2: {
    shadowOffset: { width: 1, height: 2 },
    shadowColor: '#ccc',
    width: 60,
    height: 48,
    borderColor: '#ccc'
  },
  textInputStyle3: {
    shadowOffset: { width: 1, height: 2 },
    shadowColor: '#ccc',
    width: 300,
    height: 48,
    borderColor: '#ccc'
  },
  searchSection: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  searchSection6: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  searchSection2: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  section: {
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  viewContainerStyle: {
    flexDirection: 'row',
  },
  iconContainerStyle: {
    width: 30,
    justifyContent: 'center'
  },
  textContainerStyle: {
    justifyContent: 'center'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  insta: {
    height: 150,

  },
});
