/* @flow */

import React, { Component } from 'react';
import { ActivityIndicator, Alert, TextInput, Text, Picker, ScrollView,TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';
import Iconss from 'react-native-vector-icons/dist/FontAwesome';
import CheckBox from 'react-native-checkbox';
import ModalDropdown from 'react-native-modal-dropdown';
import dismissKeyboard from 'react-native-dismiss-keyboard';

import {
  View,
  StyleSheet
} from 'react-native';
import TextInputWithIcon from '../components/TextInputWithIcon';
import Button from '../components/Button';
import SInfo from 'react-native-sensitive-info'
import PropTypes from 'prop-types';


const DEMO_OPTIONS_1 = ['Male', 'Female', 'Rather not say'];

export default class PatientRegistration extends Component {


  static navigationOptions = {
    header: null
    };



  mValidateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      firstname: '',
      lastname: '',
      password: '',
      gender: '',
      email: '',
    };
  }




  mLoaderShowHide() {
    this.setState({
      visible: !this.state.visible
    });
  };

  mValidation() {
    if (this.state.firstname.length <= 0) {
      Alert.alert('First name field is required..')
      return false;
    } else if (this.state.lastname.length <= 0) {
      Alert.alert('Last name field is required..')
      return false;
    } else if (this.state.email.length <= 0) {
      Alert.alert('Email address field is required..')
      return false;
    } else if (!this.mValidateEmail(this.state.email)) {
      Alert.alert('Please enter a valid email address.')
      return false;
    } else if (this.state.password.length <= 0) {
      Alert.alert('Password field is required..')
      return false;
    } else if (this.state.gender.length <= 0) {
      Alert.alert('Please select your gender..')
      return false;
    }
    this.mLoaderShowHide();
    this.mPatientEmailCheck();
  }

  mFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'Email address already exist');
      }, 200);
    });
  }

  mSuccess() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Success', 'Registration successfull.');
      }, 200);
    });
  }

  mNetworkFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'The Internet connection appears to be offline, Please try again');
      }, 200);
    });
  }



  mPatientEmailCheck() {
    var mThis = this;
    var data = new FormData();
    data.append("email", this.state.email);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://blog.dentalchat.com/server/service/patient/patient-email-check", true);
    xhr.onload = function () {
      var users = xhr.responseText;
      if (xhr.readyState == 4 && xhr.status == "200") {
        console.log('<><><> ' + users);
        var obj = JSON.parse(users);
        if (obj.status == "1") {
          mThis.mRegistration();
        } else {
          mThis.mFailed();
        }
      } else {
        mThis.mNetworkFailed();
        console.log('<><><> ' + users);
      }
    }
    xhr.send(data);
  }



  mRegistration() {
    if (this.state.gender == 'Male') {
      this.setState({ gender: '1' })
    } else if (this.state.gender == 'Female') {
      this.setState({ gender: '2' })
    } else if (this.state.gender == 'Rather not say') {
      this.setState({ gender: '3' })
    }
    var mThis = this;
    var data = new FormData();
    data.append("patient[first_name]", this.state.firstname);
    data.append("patient[last_name]", this.state.lastname);
    data.append("patient[password]", this.state.password);
    data.append("patient[gender]", '1');
    data.append("patient[email]", this.state.email);
    data.append("patient[mobile_request]", '1');
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://blog.dentalchat.com/server/patient-registration", true);
    xhr.onload = function () {
      var users = xhr.responseText;
      if (xhr.readyState == 4 && xhr.status == "200") {
        console.log('<><><> ' + users);
        var obj = JSON.parse(users);
        mThis.mSuccess();
        mThis.props.navigation.navigate('PatientLogin')
      } else {
        mThis.mNetworkFailed();
        console.log('<><><> ' + users);
      }
    }
    xhr.send(data);
  }

  mDemo(value) {
    dismissKeyboard()

  }


  render() {
    const { navigate } = this.props.navigation;
    return (

<View style={{backgroundColor: 'white',flex: 1}}>

<View style={{flexDirection:'row'}}>
<View style={{height:60,width:'60%',marginTop:20,marginLeft:'20%',backgroundColor:'#ffffff',justifyContent: 'center', alignItems: 'center'}}>
<Text style={{ fontSize: 18, fontWeight: 'bold' }}>Patient Registration</Text>
</View>
<View style={{position: "absolute", top: 35, left: 0,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
<View style={{ flexDirection: 'row' }}>
  <Iconss style={{ marginLeft: 15 }} name='angle-left' size={30} color={'#2b94bf'} />
  <Text style={{ fontSize: 15, marginTop: 7, marginLeft: 5, fontWeight: '500', color: '#2b94bf' }}>Back</Text>
</View>
</TouchableOpacity>
</View>
</View>

<View
style={{
borderBottomColor: 'black',
borderBottomWidth: 1,
}}
/>

<ScrollView
        style={{ backgroundColor: "#ffffff" }}
        keyboardShouldPersistTaps="never"
        keyboardDismissMode='on-drag'>
        <View style={styles.container}>
          <Spinner overlayColor={'rgba(0, 0, 0, 0.75)'} color={'#08a1d9'} textContent={"Updating"} visible={this.state.visible} textStyle={{ color: '#fff', fontSize: 15, marginTop: -70 }}>
          </Spinner>
          <View>

            <View style={styles.insta}>

            </View>

            <View style={styles.searchSection}>
              <TextInput
                keyboardType='default'
                placeholder='First Name'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ firstname: text })} />
            </View>

            <View style={styles.lineStyle}></View>

            <View style={styles.searchSection}>
              <TextInput
                keyboardType='default'
                placeholder='Last Name Initial'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ lastname: text })} />
            </View>

            <View style={styles.lineStyle}></View>

            <View style={styles.searchSection}>
              <TextInput
                keyboardType='email-address'
                placeholder='Email address'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ email: text })} />
            </View>

            <View style={styles.lineStyle}></View>

            <View style={styles.searchSection}>
              <TextInput
                secureTextEntry={true}
                keyboardType='default'
                placeholder='Password'
                style={styles.textInputStyle}
                onChangeText={(text) => this.setState({ password: text })} />
            </View>

            <View style={styles.lineStyle}></View>

            <View style={styles.searchSection}>
              <View style={styles.dropdown_row}>
                <View style={styles.dropdown_cell}>
                  <ModalDropdown style={styles.dropdown}
                    textStyle={styles.dropdown_text}
                    dropdownStyle={styles.dropdown_pop}
                    defaultValue='Choose gender'
                    dropdownTextStyle={styles.dropdown_pop_text}
                    options={DEMO_OPTIONS_1}
                    onSelect={(idx, value) => this.setState({ gender: value })} />
                </View>
              </View>
            </View>
            <View style={styles.lineStyle}></View>
            <View style={styles.buttonContainerStyle}>
              <Button
                buttonType="logInButton"
                name='Save & Continue'
                onPress={() => this.mValidation()} />
            </View>
          </View>
        </View>
      </ScrollView>

</View>

     



    );
  }
}

const styles = StyleSheet.create({


  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    backgroundColor: 'white'
  },
  dropdown: {
    flex: 1,
    top: 5,
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: 'black',
  },
  dropdown_row: {
    flexDirection: 'row',
  },
  dropdown_cell: {
    shadowOffset: { width: 1, height: 2 },
    shadowColor: '#ccc',
    width: 200,
    height: 50,
    borderColor: '#ccc'
  },
  dropdown_text: {
    marginVertical: 5,
    fontSize: 18,
    color: '#111',
    textAlignVertical: 'center',
  },
  dropdown_pop: {
    width: 200,
    height: 160,
  },
  dropdown_pop_text: {
    marginVertical: 5,
    marginHorizontal: 6,
    fontSize: 18,
    color: '#ccc',
  },

  buttonContainerStyle: {
    marginTop: 30
  },
  textInputStyle: {
    shadowOffset: { width: 1, height: 2 },
    shadowColor: '#ccc',
    width: 250,
    height: 50,
    borderColor: '#ccc'
  },
  searchSection: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  section: {
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  viewContainerStyle: {
    flexDirection: 'row',
  },
  iconContainerStyle: {
    width: 30,
    justifyContent: 'center'
  },
  textContainerStyle: {
    justifyContent: 'center'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  insta: {
    height: 150,

  },
});
