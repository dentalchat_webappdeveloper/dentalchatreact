/* @flow */

import React, { Component } from 'react';
import {
  View, StyleSheet, Image, Text
} from 'react-native';
import Button from '../components/Button';
import SInfo from 'react-native-sensitive-info';
import PropTypes from 'prop-types';

export default class Home extends Component {


  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      latitude: null,
      longitude: null,
      error: null,
      patient_login:"0",
      dentist_login:"0",
    };
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );

    SInfo.getItem('is_patient_login', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      
      if(value=="1"){
        this.props.navigation.navigate('PatientMainTab')
      }


    });

    SInfo.getItem('is_dentist_login', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      
      if(value=="1"){
        this.props.navigation.navigate('DentistTabMenu')
      }


    });


  }


  render() {
    const { navigate } = this.props.navigation;

    const showAlert = () => {
      var str = '2017-07-20 06:16:52';

      var e = new Date(str.replace(/-/g, "/"));
      var strDate = e.getDate();
      var strMonth = e.getMonth() + 1;
      var strYear = e.getFullYear();
      var strHr = e.getHours();
      var strMin = e.getMinutes();
      var strSec = e.getSeconds();
      console.log('<><><><><><>  ' + strDate);
      console.log('<><><><><><>  ' + strMonth);
      console.log('<><><><><><>  ' + strYear);
      console.log('<><><><><><>  ' + strHr);
      console.log('<><><><><><>  ' + strMin);
      console.log('<><><><><><>  ' + strSec);

      var datum = Date.parse('02/13/2017 06:16:52');
      console.disableYellowBox = true;

      //console.log("<><><><>"+new Date(Date.UTC(2017, 6, 1,0,0, 0)));
    }


    return (
      <View style={styles.viewStyle}>
        <View>
          <Image source={{ uri: 'https://image.ibb.co/mnUgSk/logo.png' }} style={styles.imageStyle} />
        </View>
        <View style={{ alignItems: 'center' }}>

          <Text style={styles.textStyle}>
            {'Who are you?'}
          </Text>



          <Button name='Patient' buttonType='homeScreenButton' onPress={() => navigate('PatientLogin')}
          />

          <Button
            name='Dentist'
            buttonType='homeScreenButton'
            onPress={() => navigate('DentistLogin')}
          />


        </View>
      </View>

    );
  }
}


const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    flexDirection: 'column',
    padding: 40,
    backgroundColor: '#2295bf',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  textContainerStyle: {
    margin: 20
  },
  textStyle: {
    color: 'white',
    fontSize: 16,
    margin: 10,
    fontWeight: '600'
  },
  imageStyle: {
    width: 200,
    height: 140,
    resizeMode: 'contain'
  }
});
