/* @flow */

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Alert,
  Text
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import DentistMessageItem from '../components/DentistMessageItem';
import { Data } from '../data/DentistMessagesList';
import Spinner from 'react-native-loading-spinner-overlay';
import MessagesIconWithBadge from '../components/MessagesIconWithBadge';
import SInfo from 'react-native-sensitive-info';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';

export default class DentistMessages extends Component {

  static navigationOptions = {
    title: 'Message',
  };



  constructor(props) {

    global.MyDental = '0';

    super(props);
    this.state = {
      visible: true,
      rady: true,
      dataArray: [],
      dataArrayAll: [],
      doctorId: '',
      isRefreshing: false,
      isSearching: false,
      alertMsg: '',
      opration: '',
      type: '',
      more: '0',
      page: 0,
      unreadCount: '0',

    };
  }

  componentWillMount() {
    this.setState({
      page: 0,
      dataArray: [],
      dataArrayAll: [],
    })
    SInfo.getItem('dentist_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      this.setState({ doctorId: value, })
    });
    SInfo.getItem('type', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      this.setState({ type: value, })
    });
    SInfo.getItem('opration', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      this.setState({ opration: value, })
      if (value == '-1') {
        this.setState({ rady: false})
        this.mGetRecentPost();
      } else if (value == '0') {
        this.setState({ rady: false})
        this.mGetUnattendantPost();
      } else if (value == '1') {
        this.setState({ rady: false})
        this.mGetAttendantPost();
      }
    });
  }

  mLoaderShowHide() {
    this.setState({
      visible: !this.state.visible
    });
  };




  mGetUnattendantPost() {
    var mThis = this;
    var rawData = [];
    var mUnreadCount = 0;

    fetch('https://blog.dentalchat.com/server/get-unattendant-post', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        auth_token:"",
        doctor_id:this.state.doctorId,
        page:this.state.page,
        perpage:'13',
        emergency:this.state.type,
      }),
    }).then((response) => response.json())
        .then((responseJson) => {
          //var users = responseJson;
         
          for (i in responseJson.patient_posts) {


            if (responseJson.patient_posts[i].chat_history_count > 0) {
              if (responseJson.patient_posts[i].chat_history_arr.doctor_content.length <= 0) {
                mUnreadCount = mUnreadCount + responseJson.patient_posts[i].unread_chat_history_count;
                console.log('<><><>### ' + mUnreadCount);
              }
            }

            if (responseJson.patient_posts[i].get_patient == null) {
            } else {
              rawData.push(responseJson.patient_posts[i])
            }
            global.MyDental = mUnreadCount;
            mThis.props.navigation.setParams({ handleSave: mUnreadCount });
            SInfo.setItem('msgCount', mUnreadCount + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });


            mThis.setState({
              more: responseJson.more,
              dataArray: [...mThis.state.dataArray, ...rawData],
              dataArrayAll: [...mThis.state.dataArrayAll, ...rawData],
              rady: true,
              visible: false
            });


           
          }
          //console.log("<><><>akki "+responseJson);
        })
        .catch((error) => {
          console.error(error);
          mThis.setState({
            rady: true,
            visible: false
          });
          mThis.mFailed();
        });






    
    
  }




  mGetAttendantPost() {
    var mThis = this;
    var rawData = [];
    var mUnreadCount = 0;

    fetch('https://blog.dentalchat.com/server/get-attendant-post', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        auth_token:"",
        doctor_id:this.state.doctorId,
        page:this.state.page,
        perpage:'13',
        emergency:this.state.type,
      }),
    }).then((response) => response.json())
        .then((responseJson) => {
          //var users = responseJson;
         
          for (i in responseJson.patient_posts) {


            if (responseJson.patient_posts[i].chat_history_count > 0) {
              if (responseJson.patient_posts[i].chat_history_arr.doctor_content.length <= 0) {
                mUnreadCount = mUnreadCount + responseJson.patient_posts[i].unread_chat_history_count;
                console.log('<><><>### ' + mUnreadCount);
              }
            }

            if (responseJson.patient_posts[i].get_patient == null) {
            } else {
              rawData.push(responseJson.patient_posts[i])
            }

            mThis.props.navigation.setParams({ handleSave: mUnreadCount });
            SInfo.setItem('msgCount', mUnreadCount + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
            mThis.setState({
              more: responseJson.more,
              dataArray: [...mThis.state.dataArray, ...rawData],
              dataArrayAll: [...mThis.state.dataArrayAll, ...rawData],
              rady: true,
              visible: false
            });


           
          }
          //console.log("<><><>akki "+responseJson);
        })
        .catch((error) => {
          console.error(error);
          mThis.setState({
            rady: true,
            visible: false
          });
          mThis.mFailed();
        });






    
    
  }





 



  mGetRecentPost() {
    var mThis = this;
    var rawData = [];
    var mUnreadCount = 0;

    fetch('https://blog.dentalchat.com/server/get-patients-post', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        auth_token:"",
        doctor_id:this.state.doctorId,
        page:this.state.page,
        perpage:'13',
      }),
    }).then((response) => response.json())
        .then((responseJson) => {
          //var users = responseJson;
         
          for (i in responseJson.patient_posts) {


            if (responseJson.patient_posts[i].chat_history_count > 0) {
              if (responseJson.patient_posts[i].chat_history_arr.doctor_content.length <= 0) {
                mUnreadCount = mUnreadCount + responseJson.patient_posts[i].unread_chat_history_count;
                console.log('<><><>### ' + mUnreadCount);
              }
            }

            if (responseJson.patient_posts[i].get_patient == null) {
            } else {
              rawData.push(responseJson.patient_posts[i])
            }

            mThis.props.navigation.setParams({ handleSave: mUnreadCount });
            SInfo.setItem('msgCount', mUnreadCount + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
            mThis.setState({
              more: responseJson.more,
              dataArray: [...mThis.state.dataArray, ...rawData],
              dataArrayAll: [...mThis.state.dataArrayAll, ...rawData],
              rady: true,
              visible: false
            });


           
          }
          //console.log("<><><>akki "+responseJson);
        })
        .catch((error) => {
          console.error(error);
          mThis.setState({
            rady: true,
            visible: false
          });
          mThis.mFailed();
        });






    
    
  }







  mFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'The Internet connection appears to be offline, Please try again');
      }, 200);
    });
  }


  

  mSearchFilterFunction(text) {
    const newData = this.state.dataArray.filter(function (item) {
      const itemData = item.post_title.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1
    })
    if (!text || text === '') {
      console.log('<><><><><>change state');
      this.setState({
        dataArray: this.state.dataArrayAll,
        text: text,
        isSearching: false
      });
    }
    else if (!Array.isArray(newData) && !newData.length) {
      this.setState({
        data: [],
        isSearching: true
      });
    } else {
      this.setState({
        dataArray: newData,
        text: text,
        isSearching: true
      })
    }
  }



  mRefresh = async () => {
    SInfo.setItem('opration', '-1', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
    SInfo.setItem('type', '-1', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
    this.setState({
      page: 0,
      rady: false,
      visible: true,
      dataArray: [],
      dataArrayAll: [],
    });
    this.mGetRecentPost();
  };



  renderHeader = () => {
    return (

      <SearchBar
      placeholder="Search here ..."
      containerStyle={{ backgroundColor: 'white' }}
      inputStyle={{ color: '#000' }}
      lightTheme
      onChangeText={(text) => this.mSearchFilterFunction(text)}
      value={this.state.text}
    />


      
    );
  };


  onScrollHandler = () => {
    if (this.state.more == 1 && this.state.isSearching == false) {

      this.setState({
        page: this.state.page + 15,
        rady: false,
        visible: true,
     }, () => {

      SInfo.getItem('dentist_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
        this.setState({ doctorId: value, })
      });
      SInfo.getItem('type', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
        this.setState({ type: value, })
      });
      SInfo.getItem('opration', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
        this.setState({ opration: value, })
        if (value == '-1') {
          this.mGetRecentPost();
        } else if (value == '0') {
          this.mGetUnattendantPost();
        } else if (value == '1') {
          this.mGetAttendantPost();
        }
      });


     });

    }
   
}



  render() {
    const { navigate } = this.props.navigation;
        return (
        <View style={styles.container}>
         {this.state.rady==false ? <Spinner overlayColor={'rgba(0, 0, 0, 0.75)'} color={'#08a1d9'} textContent={"Updating"} visible={this.state.visible} textStyle={{ color: '#fff', fontSize: 15, marginTop: -70 }} /> : <View>
            </View> } 
          <FlatList
            onRefresh={this.mRefresh}
            refreshing={this.state.isRefreshing}
            data={this.state.dataArray}
            keyExtractor={item => item.post_id}
            ListHeaderComponent={this.renderHeader}
            onEndReached={this.onScrollHandler}
            onEndReachedThreshold={0}
            renderItem={({ item }) => <DentistMessageItem item={item} onPress={() => navigate('DentistChatWindow', item)} />}
          />
        </View>
      );
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
