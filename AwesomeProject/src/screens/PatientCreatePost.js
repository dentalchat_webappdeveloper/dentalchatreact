/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Platform,
  Image,
  Alert,
  ScrollView,
  AppRegistry,
  TouchableOpacity,
  Keyboard,
  Button
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SInfo from 'react-native-sensitive-info'
import {  Avatar, Slider } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import ExpandingTextInput from '../components/ExpandingTextInput';
import PropTypes from 'prop-types';
import ToggleSwitch from 'toggle-switch-react-native'



var options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class PatientCreatePost extends Component {


  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'New Post',
      tabBarVisible: false,
      tabBarLabel: 'More',
      tabBarIcon: ({ tintColor }) => (
        <Icon name='ellipsis-h' size={30} color={tintColor} />
      ),
      headerRight: <Button onPress={() => params.handleSave()}  style={{ marginRight: 10 }} title='Done' color='#2b94bf' />,
      
      headerLeft: <TouchableOpacity onPress={() => navigation.goBack()} >
        <View style={{ flexDirection: 'row' }}>
          <Icon style={{ marginLeft: 15 }} name='angle-left' size={30} color={'#2b94bf'} />
          <Text style={{ fontSize: 15, marginTop: 7, marginLeft: 5, fontWeight: '500', color: '#2b94bf' }}>Back</Text>
        </View>
      </TouchableOpacity>
    };
  };




  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      avatarSource: '',
      patientId: '',
      pic: [],
      postTitle: '',
      currentLocation: '',
      description: '',
      latitude: '',
      longitude: '',
      error: '',
      height: 0,
      painLevel: 0,
      emergency: false,
      sendEmergency: false,

      insurance: false,
      sendInsurance: false,

      appointment: false,
      sendAppointment: false,
      tog:false,


    };
  }


  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log("<><><>1234 " + position.coords.latitude);
        console.log("<><><>1234 " + position.coords.longitude);
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
        if (this.state.latitude.length > 0 || this.state.latitude != undefined) {
          this.mLoaderShowHide();
          this.mGetCurrentLocation();
        }
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
    this.props.navigation.setParams({ handleSave: this.mValidation });
    SInfo.getItem('patient_id', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      this.setState({ patientId: value, })
    });
  }


  mValidation = () => {
    if (this.state.postTitle.length <= 0) {
      Alert.alert('post title is required.')
      return false;
    } else if (this.state.currentLocation.length <= 0) {
      Alert.alert('current location is required.')
      return false;
    } else if (this.state.description.length <= 0) {
      Alert.alert('description is required.')
      return false;
    }
    this.mLoaderShowHide();
    this.mUpdateProfile();
  }


  renderCustomActions() {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        const myImg = {
          uri: response.uri,
          type: 'image/jpeg',
          name: response.fileName,
        };
        this.setState({
          avatarSource: source,
          pic: myImg
        });
      }
    });

  }

  mFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'Something went wrong! Try again');
      }, 200);
    });
  }

  mSuccess() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Success', 'Received your post, near by Dentist(s) will contact you soon, Thank you.');
      }, 200);
    });
  }

  mLoaderShowHide() {
    this.setState({
      visible: !this.state.visible
    });
  };

  mNetworkFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'The Internet connection appears to be offline, Please try again');
      }, 200);
    });
  }

  mGetCurrentLocation() {
    var mThis = this;
    var data = new FormData();
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        mThis.mLoaderShowHide();
        console.log("<><><>1234 " + this.responseText);
        var text = this.responseText;
        console.log('<><><>abc1' + this.responseText);
        var obj = JSON.parse(text);
        for (i in obj.results) {
          console.log('<><><>abc12 ' + obj.results[0].formatted_address);
          mThis.setState({
            currentLocation: obj.results[0].formatted_address
          });
        }
      }
    });
    xhr.open("GET", "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + this.state.latitude + "," + this.state.longitude + "&sensor=true");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("postman-token", "66714504-4f88-2cce-6e47-4bd53dc4de0d");
    xhr.send(data);
  }



  mUpdateProfile() {
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    var emergency = '0';
    var appointment = '0';
    var insurance = '0';
    if(this.state.sendEmergency==true){
    emergency='1';
    }
    if(this.state.sendAppointment==true){
    appointment='1';
    }
    if(this.state.sendInsurance==true){
    insurance='1';
    }
    var data = new FormData();
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    data.append("patient_id", this.state.patientId);
    data.append("attachments[]", this.state.pic);
    data.append("auth_token", '');
    data.append("emergency",emergency);
    data.append("post_title", this.state.postTitle);
    data.append("address", this.state.currentLocation);
    data.append("pain_level", this.state.painLevel);
    data.append("description", this.state.description);
    data.append("appointment",appointment);
    data.append("appointment_time", '1');
    data.append("insurance",insurance);
    data.append("posted_date",year+"-"+month+"-"+date+" "+hours+":"+min+":"+sec);


   fetch('https://blog.dentalchat.com/server/create-patient-post', {
      method: 'POST',
      body: data,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => response.json())
        .then((responseJson) => {
          //var users = responseJson;
            this.mLoaderShowHide();
            if (responseJson.status == 1) {
              this.mSuccess();
              this.props.navigation.goBack();
            } else {
              mThis.mFailed();
            }
        })
        .catch((error) => {
          this.mLoaderShowHide();
          console.error(error);
          this.mFailed();
        });

  }



  handleKeyDown = function (e) {
    if (e.nativeEvent.key == "Enter") {
      console.log('<><><><>enter press here! ')
      Keyboard.dismiss();
    } else {
      console.log('<><><><>enter press here5555! ')
    }
  }

  _scrollToInput() {
    const scrollResponder = this.refs.myScrollView.getScrollResponder();
    const inputHandle = React.findNodeHandle(this.refs.myInput)

    scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
      inputHandle, // The TextInput node handle
      0, // The scroll view's bottom "contentInset" (default 0)
      true // Prevent negative scrolling
    );
  }

  toggleSwitchAppoinment() {
  this.setState({
  sendAppointment: !this.state.sendAppointment
  });
  }

  toggleSwitchSendEmergency() {
    this.setState({
      sendEmergency: !this.state.sendEmergency
    });
    }

    toggleSwitchSendInsurance() {
      this.setState({
        sendInsurance: !this.state.sendInsurance
      });
      }

  render() {
    const { navigate } = this.props.navigation;


    return (
      <View style={styles.container}>
        <Spinner overlayColor={'rgba(0, 0, 0, 0.75)'} color={'#08a1d9'} textContent={"Updating"} visible={this.state.visible} textStyle={{ color: '#fff', fontSize: 15, marginTop: -70 }}>
        </Spinner>

        <ScrollView
          keyboardShouldPersistTaps="never"
          keyboardDismissMode='on-drag'>

          <View style={{ backgroundColor: '#ffffff' }}>

            <View style={styles.editViewNext}>
              <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#fff', marginTop: 5, marginBottom: 5 }}>
                <Text style={{ marginLeft: 20,marginRight:20, fontSize: 16, color: '#2b94bf', fontWeight: 'bold' }}>Post Title</Text>
              </View>
              <View style={styles.searchSection}>
                  <TextInput
                  maxLength={200}
                  value={this.state.postTitle}
                  keyboardType='default'
                  placeholder='not added'
                  style={styles.textInputStyle}
                  onChangeText={(text) => this.setState({ postTitle: text })} />
              </View>
              <View style={{ marginLeft: 20,marginRight:20, borderWidth: 0.5, marginTop: 10, borderColor: '#cccccc', }}></View>
            </View>

            <View style={{ backgroundColor: '#fff', marginTop: 10, }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#fff', marginTop: 5, marginBottom: 5 }}>
                <Text style={{ marginLeft: 20,marginRight:20, fontSize: 16, color: '#2b94bf', fontWeight: 'bold' }}>Current Location</Text>
              </View>
              <View style={styles.searchSection}>
                <TextInput
                  keyboardType='default'
                  placeholder='not added'
                  style={styles.textInputStyle}
                  value={this.state.currentLocation}
                  onChangeText={(text) => this.setState({ currentLocation: text })} />
              </View>
              <View style={{ marginLeft: 20,marginRight:20, borderWidth: 0.5, marginTop: 10, borderColor: '#cccccc', }}></View>
            </View>

            <View style={{ backgroundColor: '#fff', marginTop: 10, }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#fff', marginTop: 5, marginBottom: 5 }}>
                <Text style={{ marginLeft: 20,marginRight:20, fontSize: 16, color: '#2b94bf', fontWeight: 'bold' }}>Description</Text>
              </View>
              <View style={styles.searchSection}>

                <TextInput
                  maxLength={500}
                  style={styles.textInputStyleDesc}
                  multiline={true}
                  onChangeText={(text) => this.setState({ description: text })}
                  keyboardType="default"
                  returnKeyType="done"
                  onKeyPress={this.handleKeyDown}
                  placeholder='not added'
                />


              </View>
              <View style={{ marginLeft: 20,marginRight:20, borderWidth: 0.5, marginTop: 10, borderColor: '#cccccc', }}></View>
            </View>




            <View style={{ backgroundColor: '#fff', marginTop: 10, }}>

            <View style={styles.searchSection}>
            <Text style={{ marginLeft: 20, fontSize: 16, color: '#2b94bf', fontWeight: 'bold' }}>Pain Level ({this.state.painLevel}) </Text>
            
              </View>

            <View style={styles.searchSection}>

            <View style={{flexDirection: 'row',width:'100%' }}>
                    <View style={{alignItems: "flex-start",width:'33%'}}>
                    <Text style={{ fontSize: 16,marginLeft:20, color: '#2b94bf', fontWeight: 'bold'}}>Low</Text>
                    
</View>
                    <View style={{alignItems: "center",width:'34%'}}>
                    <Text style={{ fontSize: 16, color: '#2b94bf', fontWeight: 'bold'}}>Medium</Text>
                    
</View>
                    <View style={{alignItems: 'flex-end',width:'33%'}}>
                    <Text style={{ fontSize: 16,marginRight:20, color: '#2b94bf', fontWeight: 'bold'}}>High</Text>
                    
</View>
                      
                  </View>

              </View>


              <View style={styles.searchSection}>

          


              <Slider
                    step={1}
                    minimumValue={0}
                    maximumValue={10}
                    minimumTrackTintColor="#2b94bf"
                    thumbTintColor={"#2b94bf"}
                    onValueChange={(ChangedValue) => this.setState({ painLevel: ChangedValue })}
                    style={{ width: '90%',marginLeft:20,marginRight:20 }} />


                
                  
                  
                



                

              </View>
              <View style={{ marginLeft: 20,marginRight:20, borderWidth: 0.5, marginTop: 10, borderColor: '#cccccc', }}></View>
            </View>


            <View style={{ backgroundColor: '#fff', marginTop: 30, }}>
              <View style={{ width: "100%", flexDirection: 'row', }}>
                <View style={{ width: "70%" }}>
                  {this.state.sendEmergency === true ? <Text style={{ marginLeft: 20, fontSize: 16, color: '#ff0000', fontWeight: 'bold' }}>Emergency (Yes)</Text> : <Text style={{ marginLeft: 20, fontSize: 16, color: '#fda629', fontWeight: 'bold' }}>Emergency (No)</Text>}
                </View>
                <View style={{ width: 100, justifyContent: 'flex-end' }}>
                <View style={{ alignSelf: 'flex-end' }}>

                <ToggleSwitch
                    isOn={this.state.sendEmergency}
                    onColor='#ff0000' 
                    offColor='#fda629' 
                    size='medium' 
                    onToggle={(isOn) => this.toggleSwitchSendEmergency()} />
                
                  </View>
                </View>
              </View>
              <View style={{ marginLeft: 20,marginRight:20, borderWidth: 0.5, marginTop: 10, borderColor: '#cccccc', }}></View>
            </View>


            <View style={{ backgroundColor: '#fff', marginTop: 30, }}>
              <View style={{ width: "100%", flexDirection: 'row', }}>
                <View style={{ width: "70%" }}>
                  {this.state.sendAppointment === true ? <Text style={{ marginLeft: 20, fontSize: 16, color: '#ff0000', fontWeight: 'bold' }}>Are you looking for ‘Same day or walk-in appointment’? (Yes)</Text> : <Text style={{ marginLeft: 20, fontSize: 16, color: '#fda629', fontWeight: 'bold' }}>Are you looking for ‘Same day or walk-in appointment’? (No)</Text>}
                </View>
                <View style={{ width: 100, justifyContent: 'flex-end' }}>
                  <View style={{ alignSelf: 'flex-end' }}>
                    <ToggleSwitch
                    isOn={this.state.sendAppointment}
                    onColor='#ff0000' 
                    offColor='#fda629' 
                    size='medium' 
                    onToggle={(isOn) => this.toggleSwitchAppoinment()} />
                  </View>
                </View>
              </View>
              <View style={{ marginLeft: 20,marginRight:20, borderWidth: 0.5, marginTop: 10, borderColor: '#cccccc', }}></View>
            </View>

            <View style={{ backgroundColor: '#fff', marginTop: 30, }}>
              <View style={{ width: "100%", flexDirection: 'row', }}>
                <View style={{ width: "70%" }}>
                  {this.state.sendInsurance === true ? <Text style={{ marginLeft: 20, fontSize: 16, color: '#ff0000', fontWeight: 'bold' }}>Do you have dental insurance ? (Yes)</Text> : <Text style={{ marginLeft: 20, fontSize: 16, color: '#fda629', fontWeight: 'bold' }}>Do you have dental insurance ? (No)</Text>}
                </View>
                <View style={{ width: 100, justifyContent: 'flex-end' }}>
                  <View style={{ alignSelf: 'flex-end' }}>
                    <ToggleSwitch 
                    isOn={this.state.sendInsurance} 
                    onColor='#ff0000' 
                    offColor='#fda629' 
                    size='medium' 
                    onToggle={(isOn) => this.toggleSwitchSendInsurance() } />
                  </View>
                </View>
              </View>
              <View style={{ marginLeft: 20,marginRight:20, borderWidth: 0.5, marginTop: 10, borderColor: '#cccccc', }}></View>
            </View>







          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
            <Text style={{ marginLeft: 20, fontSize: 16, color: '#2b94bf', fontWeight: 'bold' }}>Add Photo</Text>
          </View>
          <View style={styles.profileImage}>
            <Avatar onPress={() => this.renderCustomActions()} large source={this.state.avatarSource} />
          </View>




        </ScrollView>



      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchIcon: {
    marginLeft: 20
  },
  textInputStyle: {
    width: "90%",
    marginLeft: 20,
    marginRight: 20,
    fontSize: 17,
    fontWeight: '500'
  },
  textInputStyle: {
    width: "90%",
    marginLeft: 20,
    marginRight: 20,
    fontSize: 17,
    fontWeight: '500'
  },
  textInputStyleDesc: {
    width: "90%",
    height: 70,
    marginLeft: 20,
    marginRight: 20,
    fontSize: 17,
    fontWeight: '500'
  },
  logoutContainer: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  optionsListStyle: {
    marginTop: 10,
    marginBottom: 30,
    backgroundColor: 'white'
  },
  subtitleView: {
    marginLeft: 30,
  },
  maintitleView: {
    marginLeft: 30,
    marginTop: 10
  },
  optiontitleView: {
    marginTop: 10,
  },
  subtitleTextStyle: {
    color: '#999'
  },
  maintitleTextStyle: {
    color: '#111',
    fontSize: 18,
    fontWeight: 'bold',
  },
  titleAndVersionContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150
  },
  titleContainerStyle: {
    marginBottom: 10
  },
  titleTextStyle: {
    fontWeight: 'bold',
    fontSize: 15,
    color: 'grey',
  },
  versionTextStyle: {
    color: 'grey'
  },
  containerRow: {
    height: 60,
    marginTop: 10,
    backgroundColor: 'white'
  },
  imageview: {
    marginTop: 5
  },
  profileImage: {
    marginTop: 10,
    marginLeft: 20
  },
  editView: {
    height: 50,
    backgroundColor: '#fff',
    marginTop: 10,
  },
  editViewNext: {
    backgroundColor: '#fff',
    marginTop: 1,
  },
  editViewLast: {
    height: 50,
    backgroundColor: '#fff',
    marginTop: 60,
  },
  searchSection: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  dropdown: {
    width: 300,
    height: 50,
    marginLeft: 20,
  },
  dropdown_text: {
    marginTop: 14,
    fontSize: 18,
    fontWeight: '500',
    color: '#111',
  },
  dropdown_pop: {
    width: 200,
    height: 100,
  },
  dropdown_pop_text: {
    marginVertical: 5,
    marginHorizontal: 6,
    fontSize: 18,
    color: '#ccc',
  },
});


const optionsList = [
  {
    title: 'Edit Profie',
    icon: 'edit',
    screenName: 'DentistEditProfile'
  },
  {
    title: 'Change Password',
    icon: 'lock',
    screenName: 'DentistChangePassword'
  },
];
