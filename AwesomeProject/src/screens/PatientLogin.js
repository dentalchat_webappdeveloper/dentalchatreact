/* @flow */

import React, { Component } from 'react';
import { ActivityIndicator, Alert, TextInput, Text,TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';

import Iconss from 'react-native-vector-icons/dist/FontAwesome';
import CheckBox from 'react-native-checkbox';


import {
  View,
  StyleSheet
} from 'react-native';
import TextInputWithIcon from '../components/TextInputWithIcon';
import Button from '../components/Button';
import SInfo from 'react-native-sensitive-info';
import PropTypes from 'prop-types';




export default class PatientLogin extends Component {


  static navigationOptions = {
    header: null
    };



 
 


  mValidateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      username: '',
      password: '',
      checkBoxValue: '',
      checkBox: false,

    };
  }

  componentWillMount() {
    var mt = true;
    SInfo.getItem('login_email', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' }).then(value => {
      if (value == undefined) {
        value = "";
        mt = false;
      }
      this.setState({ username: value, checkBox: mt })
    });
  }

  mLoaderShowHide() {
    this.setState({
      visible: !this.state.visible
    });
  };

  mCheckBox() {
    this.setState({
      checkBox: !this.state.checkBox,
    })
  }

  mValidation() {
    if (this.state.username.length <= 0) {
      Alert.alert('Email address is required.')
      return false;
    } else if (!this.mValidateEmail(this.state.username)) {
      Alert.alert('Please enter a valid email address.')
      return false;
    } else if (this.state.password.length <= 0) {
      Alert.alert('Password is required.')
      return false;
    }
    this.mLoaderShowHide();
    this.mLogin();
  }

  mFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'Please provide valid DentalChat email account or password');
      }, 200);
    });
  }


  mNetworkFailed() {
    this.setState({ visible: false }, () => {
      setTimeout(() => {
        Alert.alert('Failed', 'Something went wrong, Please try again');
      }, 200);
    });
  }


  mLogin() {
    const { navigate } = this.props.navigation;
    var mThis = this;
    var data = new FormData();
    data.append("email", this.state.username);
    data.append("password", this.state.password);
    var xhr = new XMLHttpRequest(); 
    xhr.open("POST", "https://blog.dentalchat.com/server/patient-login", true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = function () {
      mThis.mLoaderShowHide();
      var users = xhr.responseText;
      console.log('<><><> ' + users);
      if (xhr.readyState == 4 && xhr.status == "200") {
        //console.log('<><><> '+users);
        var obj = JSON.parse(users);
        if (obj.status == 1) {
          SInfo.setItem('patient_id', obj.patient_id_raw + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          SInfo.setItem('patient_name', obj.patient_name + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          SInfo.setItem('patient_pic', obj.profile_pic + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          SInfo.setItem('patient_tokan', obj.auth_token + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          SInfo.setItem('patient_email', mThis.state.username + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          SInfo.setItem('patient_password', mThis.state.password + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          SInfo.setItem('post_id', '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          SInfo.setItem('is_patient_login', '1', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          navigate('PatientMainTab')
          if (mThis.state.checkBox == true) {
            SInfo.setItem('login_email', mThis.state.username + '', { sharedPreferencesName: 'mySharedPrefs', keychainService: 'myKeychain' });
          }
        } else {
          mThis.mFailed();
        }
      } else {
        mThis.mNetworkFailed();
      }
    }
    xhr.send(data);
  }


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={{backgroundColor: 'white',flex: 1}}>


<View style={{flexDirection:'row'}}>
<View style={{height:60,width:'60%',marginTop:20,marginLeft:'20%',backgroundColor:'#ffffff',justifyContent: 'center', alignItems: 'center'}}>
<Text style={{ fontSize: 18, fontWeight: 'bold' }}>Patient Login</Text>
</View>
<View style={{position: "absolute", top: 35, left: 0,justifyContent: 'center', alignItems: 'center'}}>
<TouchableOpacity onPress={() => this.props.navigation.goBack()}>
<View style={{ flexDirection: 'row' }}>
  <Iconss style={{ marginLeft: 15 }} name='angle-left' size={30} color={'#2b94bf'} />
  <Text style={{ fontSize: 15, marginTop: 7, marginLeft: 5, fontWeight: '500', color: '#2b94bf' }}>Back</Text>
</View>
</TouchableOpacity>
</View>
</View>

<View
style={{
borderBottomColor: 'black',
borderBottomWidth: 1,
}}
/>


      <View style={styles.container}>
        <Spinner overlayColor={'rgba(0, 0, 0, 0.75)'} color={'#08a1d9'} textContent={"Establishing secure connection"} visible={this.state.visible} textStyle={{ color: '#fff', fontSize: 15, marginTop: -70 }}>
        </Spinner>
        <View>

       

          <View style={styles.searchSection}>
            <Icon style={styles.searchIcon} name="md-at" size={24} color="#BDC6CF" />
            <TextInput
              value={this.state.username}
              keyboardType='email-address'
              placeholder='Email address'
              style={styles.textInputStyle}
              onChangeText={(text) => this.setState({ username: text })} />
          </View>

          <View style={styles.searchSection}>
            <Icon style={styles.searchIcon} name="md-lock" size={24} color="#BDC6CF" />

            <TextInput
              secureTextEntry={true}
              keyboardType='default'
              placeholder='Password'
              style={styles.textInputStyle}
              onChangeText={(text) => this.setState({ password: text })} />
          </View>

        </View>
        <View style={styles.buttonContainerStyle}>
          <Button
            buttonType="logInButton"
            name='Sign In'
            onPress={() => this.mValidation()} />
        </View>



        <View style={styles.section}>

          <CheckBox
            checkboxStyle={{ height: 15, width: 15 }}
            containerStyle={{ marginTop: 12, paddingLeft: 40 }}
            label='Remember Me'
            labelStyle={{ color: '#999999' }}
            checked={this.state.checkBox}
            onChange={() => this.mCheckBox()}
          />

          <Button
            name='Forgot Password ?'
            onPress={() => navigate('PatientForgetPassword')} />
        </View>

        <Button
          name='New to DentalChat ?' />


        <Button
          buttonType="logInButton"
          name='Sign up'
          onPress={() => navigate('PatientRegistration')} />

      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({


  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    backgroundColor: 'white'
  },
  buttonContainerStyle: {
    marginTop: 30
  },

  searchSection: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  section: {
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  viewContainerStyle: {
    flexDirection: 'row',
  },
  iconContainerStyle: {
    width: 30,
    justifyContent: 'center'
  },
  textContainerStyle: {
    justifyContent: 'center'
  },
  textInputStyle: {
    shadowOffset: { width: 1, height: 2 },
    shadowColor: '#ccc',
    width: 300,
    height: 50,
    paddingLeft: 5
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
