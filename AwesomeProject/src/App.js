/* @flow */

import React, { Component } from 'react';
import {Text} from 'react-native';
import { createStackNavigator, createBottomTabNavigator,createAppContainer } from 'react-navigation';
import Home from './screens/Home';
import DentistLogin from './screens/DentistLogin';
import DentistDashboard from './screens/DentistDashboard';
import DentistMessages from './screens/DentistMessages';
import DentistAccount from './screens/DentistAccount';
import DentistChatWindow from './screens/DentistChatWindow';
import PostDetails from './screens/PostDetails';
import DentistEditProfile from './screens/DentistEditProfile';
import DentistChangePassword from './screens/DentistChangePassword';
import DentistForgotPassword from './screens/DentistForgotPassword';
import PatientRegistration from './screens/PatientRegistration';
import PatientForgetPassword from './screens/PatientForgetPassword';
import PatientAccount from './screens/PatientAccount';
import PatientProfileUpdate from './screens/PatientProfileUpdate';
import PatientLogin from './screens/PatientLogin';
import PatientDashBoard from './screens/PatientDashBoard';
import PatientMessages from './screens/PatientMessages';
import PatientChatWindow from './screens/PatientChatWindow';
import PatientPostDetails from './screens/PatientPostDetails';
import PatientDoctorList from './screens/PatientDoctorList';
import PatientCreatePost from './screens/PatientCreatePost';
import PatientDoctorProfile from './screens/PatientDoctorProfile';
import PatientPostDetailsFromList from './screens/PatientPostDetailsFromList';
import PatientChangePassword from './screens/PatientChangePassword';
import DentistRegistration from './screens/DentistRegistration';
import DentistProfileUpdate from './screens/DentistProfileUpdate';
import Ionicons from 'react-native-vector-icons/Ionicons';
import IconBadge from 'react-native-icon-badge';
import Icon from 'react-native-vector-icons/dist/FontAwesome';





//#############PATIENT

const PatientAccountStack = createStackNavigator({
  PatientAccount: {
     screen: PatientAccount,
   },
   PatientChangePassword:{
     screen:PatientChangePassword,
  },
  PatientProfileUpdate:{
    screen:PatientProfileUpdate,
  },
  },{
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
  });




 



const PatientDashBoardStack = createStackNavigator({
  PatientDashBoard: {
     screen: PatientDashBoard 
   },
   PatientDoctorList:{
     screen:PatientDoctorList
  },
  PatientDoctorProfile:{
    screen:PatientDoctorProfile
  },
  PatientCreatePost:{
  screen:PatientCreatePost  
  },
  PatientPostDetailsFromList:{
    screen:PatientPostDetailsFromList
  },
  PatientMessages: {
    screen: PatientMessages 
  },

},{
  defaultNavigationOptions: {
    gesturesEnabled: false,
  },
}

);

const PatientMessagesStack = createStackNavigator({
  PatientMessages: {
     screen: PatientMessages 
   },
   PatientProfileUpdate: {
    screen: PatientProfileUpdate 
  }
},{
  defaultNavigationOptions: {
    gesturesEnabled: false,
  },
});

const PatientMainTab = createBottomTabNavigator({
  Message: {
      screen: PatientMessagesStack,
      navigationOptions: ({navigation}) => ({
        
        title:'Message',
          headerTintColor: 'white',
          
          tabBarIcon: ({focused, tintColor}) => {
              return (
                <IconBadge
                MainElement={ <Ionicons
                      name='ios-mail'
                      type='ionicon'
                      color={tintColor}
                      size={30}
                  />}
                BadgeElement={<Text style={{ color: 'white',fontSize: 12, }}> {global.MyVar}</Text>}
                IconBadgeStyle={
      {width:18,
      top:-2,
      right:-6,
      height:18,
      backgroundColor: '#FF0000',}
    }
              />
              );
          },

      })
  },
  Dashboard: {
      screen: PatientDashBoardStack,
      navigationOptions: ({navigation}) => ({
        title:'Dashboard',
          headerTintColor: 'white',
          tabBarIcon: ({focused, tintColor}) => {
            
              return (
                  <Ionicons
                      name='ios-list-box'
                      type='ionicon'
                      color={tintColor}
                      size={30}
                  />
              );
          },

      })
  },
  More: {
    screen: PatientAccountStack,
    navigationOptions: ({navigation}) => ({
      title:'More',
        headerTintColor: 'white',
        tabBarIcon: ({focused, tintColor}) => {
          
            return (
                <Ionicons
                    name='ios-more'
                    type='ionicon'
                    color={tintColor}
                    size={30}
                />
            );
        },

    })
},



}, {
  tabBarOptions: {
      showIcon: true,
      inactiveTintColor: '#d6d6d6',
      activeTintColor: "#2b94bf"
  },

});





PatientMainTab.navigationOptions = {
  // Hide the header from AppNavigator stack
  header: null,
  title: "ddffds",

};















const PatientLoginStack = createStackNavigator(
  {
    Home: Home,
    PatientLogin: PatientLogin,
    PatientRegistration: PatientRegistration,
    PatientMainTab: PatientMainTab,
    PatientChatWindow: PatientChatWindow,
    PatientPostDetails: PatientPostDetails,
    PatientDoctorProfile: PatientDoctorProfile,
    DentistForgotPassword: DentistForgotPassword,
    PatientForgetPassword: PatientForgetPassword,
  },
  {
    initialRouteName: 'Home',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerStyle: {
        backgroundColor: '#ffffff',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);





//###########DENTIST

const DentistMessageStackNav = createStackNavigator({
  DentistMessages: {
     screen: DentistMessages 
   }
},{
  defaultNavigationOptions: {
    gesturesEnabled: false,
  },
});

const DentistDashboardStackNav = createStackNavigator({
  DentistDashboard: {
     screen: DentistDashboard
   },
}, {
  defaultNavigationOptions: {
    gesturesEnabled: false,
  },
});

const DentistAccountStackNav = createStackNavigator({
  DentistAccount: {
     screen: DentistAccount
   },
  DentistEditProfile: {
     screen: DentistEditProfile
  },
  DentistProfileUpdate: {
    screen: DentistProfileUpdate
  },
  DentistChangePassword: {
     screen: DentistChangePassword
  }
}, {
  defaultNavigationOptions: {
    gesturesEnabled: false,
  },
});


const DentistTabMenu = createBottomTabNavigator({
  Dashboard: {
      screen: DentistDashboardStackNav,
      navigationOptions: ({navigation}) => ({
        title:'Dashboard',
          headerTintColor: 'white',
          tabBarIcon: ({focused, tintColor}) => {
            return (
              <Ionicons
                  name='ios-list-box'
                  type='ionicon'
                  color={tintColor}
                  size={30}
              />
          );
          },

      })
  },
  Message: {
      screen: DentistMessageStackNav,
      navigationOptions: ({navigation}) => ({
        title:'Message',
          headerTintColor: 'white',
          tabBarIcon: ({focused, tintColor}) => {
            return (
              <IconBadge
              MainElement={ <Ionicons
                    name='ios-mail'
                    type='ionicon'
                    color={tintColor}
                    size={30}
                />}
              BadgeElement={<Text style={{ color: 'white',fontSize: 12, }}>{global.MyDental}</Text>}
              IconBadgeStyle={
    {width:18,
    top:-2,
    right:-6,
    height:18,
    backgroundColor: '#FF0000',}
  }
            />
            );
            
             
          },

      })
  },
  Account: {
    screen: DentistAccountStackNav,
    navigationOptions: ({navigation}) => ({
      title:'More',
        headerTintColor: 'white',
        tabBarIcon: ({focused, tintColor}) => {
          
            return (
                <Ionicons
                    name='ios-more'
                    type='ionicon'
                    color={tintColor}
                    size={30}
                />
            );
        },

    })
},



}, {
  tabBarOptions: {
      showIcon: true,
      inactiveTintColor: '#d6d6d6',
      activeTintColor: "#2b94bf"
  },

});


DentistTabMenu.navigationOptions = {
  // Hide the header from AppNavigator stack
  header: null,
  title: "ddffds",

};



const DentistLoginStack = createStackNavigator(
  {
    Home: Home,
    DentistLogin: DentistLogin,
    DentistRegistration: DentistRegistration,
    DentistTabMenu: DentistTabMenu,
    DentistForgotPassword: DentistForgotPassword,
    PatientPostDetails: PatientPostDetails,
    DentistChatWindow: DentistChatWindow,
    PostDetails: PostDetails,
  },
  {
    initialRouteName: 'Home',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerStyle: {
        backgroundColor: '#ffffff',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

















//############COMMON###########


const SimpleApp = createStackNavigator({
  
  PatientLoginStack: {
     screen: PatientLoginStack
   },
   DentistLoginStack:{
     screen:DentistLoginStack
   },
  
},{
  defaultNavigationOptions: {
    header: null,
    gesturesEnabled: false,
  },
});

const container = createAppContainer(SimpleApp);
export default container; 



