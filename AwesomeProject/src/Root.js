/* @flow */

import React, { Component } from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';
import Home from './screens/Home';
import DentistLogin from './screens/DentistLogin';
import DentistDashboard from './screens/DentistDashboard';
import DentistMessages from './screens/DentistMessages';
import DentistAccount from './screens/DentistAccount';
import DentistChatWindow from './screens/DentistChatWindow';
import PostDetails from './screens/PostDetails';
import DentistEditProfile from './screens/DentistEditProfile';
import DentistChangePassword from './screens/DentistChangePassword';
import DentistForgotPassword from './screens/DentistForgotPassword';

import PatientLogin from './screens/PatientLogin';

const DentistMessageStackNav = StackNavigator({
  DentistMessages: {
     screen: DentistMessages
   },
  DentistChatWindow: {
    screen: DentistChatWindow,
  },
  PostDetails: {
    screen: PostDetails
  }
}, {
  navigationOptions: {
    headerBackTitleStyle: {
      color: '#2b94bf'
    },
    headerTitleStyle: {
      color: 'black'
    },
    headerTintColor: '#2b94bf',
    headerBackTitle: null
  }
});

const DentistDashboardStackNav = StackNavigator({
  DentistDashboard: {
     screen: DentistDashboard
   },
}, {
  navigationOptions: {
    headerBackTitleStyle: {
      color: '#2b94bf'
    },
    headerTitleStyle: {
      color: 'black'
    },
    headerTintColor: '#2b94bf'
  }
});

const DentistAccountStackNav = StackNavigator({
  DentistAccount: {
     screen: DentistAccount
   },
  DentistEditProfile: {
     screen: DentistEditProfile
  },
  DentistChangePassword: {
     screen: DentistChangePassword
  }
}, {
  navigationOptions: {
    headerBackTitleStyle: {
      color: '#2b94bf'
    },
    headerTitleStyle: {
      color: 'black'
    },
    headerBackTitle: null,
    headerTintColor: '#2b94bf'
  }
});

const DentistLoginStack = StackNavigator({
  Home: {
     screen: Home
   },
  DentistLogin: {
    screen: DentistLogin,
  },
  DentistForgotPassword: {
    screen: DentistForgotPassword
  },
}, {
  navigationOptions: {
    headerBackTitleStyle: {
      color: '#2b94bf'
    },
    headerTitleStyle: {
      color: 'black'
    },
    headerBackTitle: null,
    headerTintColor: '#2b94bf'
  }
});

const DentistTabMenu = TabNavigator({
  DentistDashboardStackNav: {
     screen: DentistDashboardStackNav,
   },
  DentistMessageStackNav: {
     screen: DentistMessageStackNav
   },
  DentistAccountStackNav: {
     screen: DentistAccountStackNav
   }
}, {
  tabBarOptions: {
    activeTintColor: '#2b94bf',
    style: {
      padding: 3,
    }
  }
});

const PatientLoginStack = StackNavigator({
  Home: {
     screen: Home
   },
  PatientLogin: {
    screen: PatientLogin,
  },
  DentistForgotPassword: {
    screen: DentistForgotPassword
  },
}, {
  navigationOptions: {
    headerBackTitleStyle: {
      color: '#2b94bf'
    },
    headerTitleStyle: {
      color: 'black'
    },
    headerBackTitle: null,
    headerTintColor: '#2b94bf'
  }
});

const SimpleApp = StackNavigator({
  DentistLoginStack: {
    screen: DentistLoginStack
  },
  DentistTabMenu: {
     screen: DentistTabMenu
   },
   PatientLoginStack: {
     screen: PatientLoginStack
   }
}, {
  navigationOptions: {
    headerBackTitleStyle: {
      color: '#2b94bf'
    },
    headerTitleStyle: {
      color: 'black'
    },
    headerBackTitle: null,
    headerTintColor: '#2b94bf',
    header: null,
    gesturesEnabled: false
  }
});


export default class App extends Component {
  render() {
    return (
      <SimpleApp />
    );
  }
}
